var APP_DATA = {
  scenes: [
    {
      id: "clarohigh",
      name: "claro2",
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
      ],
      faceSize: 512,
      initialViewParameters: {
        pitch: 0,
        yaw: 0,
        fov: 1.5707963267948966,
      },
      linkHotspots: [],
      infoHotspots: [
        {
          yaw: -3,
          pitch: -0.20824636049505507,
          title: "Titlex",
          text:
            '<iframe style="width:100%;height:98%" src="https://duncanjuegobuscawally.netlify.app/?user_id=${user_id}"> </iframe>',
        },
        {
          yaw: 0.544512709184243,
          pitch: -0.20824636049505507,
          title: "Title1",
          text:
            '<iframe width="100%" height="98%" src="https://player.vimeo.com/video/452637119" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        },
        {
          yaw: -0.018014715321998054,
          pitch: -0.12861602146592332,
          title: "Conoce mas",
          text:
            '<iframe width="100%" height="98%" src="https://player.vimeo.com/video/452637147" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        },
        {
          yaw: -0.6058380854972256,
          pitch: -0.1696677446062651,
          title: "Title2",
          text:
            "<a href='http://clarovideo.com' target='_blank'><img style='max-witdh:100%;width:auto;height:98%' src='img/pantalladerecha.png'></a>",
        },
        {
          yaw: -1.074,
          pitch: -0.07,
          title: "Huawei",
          text:
            "<a href='https://tienda.claro.com.co/' target='_blank'><img style='max-witdh:100%;width:auto;height:98%' src='img/BanerHuawei.jpg'></a> ",
        },
        {
          yaw: -1.18,
          pitch: -0.07,
          title: "Samsung",
          text:
            "<a href='https://tienda.claro.com.co/' target='_blank'><img style='max-witdh:100%;width:auto;height:98%' src='img/BanerSamsung.jpg'></a> ",
        },
        {
          yaw: -1.28,
          pitch: -0.07,
          title: "Tecnología",
          text:
            "<a href='https://tienda.claro.com.co/' target='_blank'><img style='max-witdh:100%;width:auto;height:98%' src='img/BanerTecnologia.jpg'></a> ",
        },
        {
          yaw: 1.19,
          pitch: -0.12,
          title: "Anni",
          text: `<iframe style="width:100%;height:98%" src="https://duncanjuegobuscawally.netlify.app/?user_id=${user_id}&refresh=true"> </iframe>`,
        },
      ],
    },
  ],
  name: "Project Title",
  settings: {
    mouseViewMode: "drag",
    autorotateEnabled: false,
    fullscreenButton: false,
    viewControlButtons: true,
  },
};
